package grpcArctur;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.sql.Date;
import java.sql.PreparedStatement;

import com.arctur.Podatek.CreateMejnikRequest;
import com.arctur.Podatek.CreateMejnikResponse;
import com.arctur.Podatek.CreateNalogaRequest;
import com.arctur.Podatek.CreateNalogaResponse;
import com.arctur.Podatek.CreateSestanekRequest;
import com.arctur.Podatek.CreateSestanekResponse;
import com.arctur.Podatek.CreateZadolzitevRequest;
import com.arctur.Podatek.CreateZadolzitevResponse;
import com.arctur.Podatek.Datum;
import com.arctur.Podatek.DeleteMejnikRequest;
import com.arctur.Podatek.DeleteMejnikResponse;
import com.arctur.Podatek.DeleteNalogaRequest;
import com.arctur.Podatek.DeleteNalogaResponse;
import com.arctur.Podatek.DeleteSestanekRequest;
import com.arctur.Podatek.DeleteSestanekResponse;
import com.arctur.Podatek.DeleteZadolzitevByOsebaRequest;
import com.arctur.Podatek.DeleteZadolzitevByOsebaResponse;
import com.arctur.Podatek.DeleteZadolzitevRequest;
import com.arctur.Podatek.DeleteZadolzitevResponse;
import com.arctur.Podatek.DodajZaposlenegaRequest;
import com.arctur.Podatek.DodajZaposlenegaResponse;
import com.arctur.Podatek.FilterByDodeljeneRequest;
import com.arctur.Podatek.FilterByDodeljeneResponse;
import com.arctur.Podatek.FilterByNazivRequest;
import com.arctur.Podatek.FilterByNazivResponse;
import com.arctur.Podatek.FilterByOpravljeneRequest;
import com.arctur.Podatek.FilterByOpravljeneResponse;
import com.arctur.Podatek.FilterByOsebaRequest;
import com.arctur.Podatek.FilterByOsebaResponse;
import com.arctur.Podatek.Mejnik;
import com.arctur.Podatek.Naloga;
import com.arctur.Podatek.OdstraniZaposlenegaRequest;
import com.arctur.Podatek.OdstraniZaposlenegaResponse;
import com.arctur.Podatek.Opravek;
import com.arctur.Podatek.PridobiZaposlenegaRequest;
import com.arctur.Podatek.PridobiZaposlenegaResponse;
import com.arctur.Podatek.ReadAllMejnikRequest;
import com.arctur.Podatek.ReadAllMejnikResponse;
import com.arctur.Podatek.ReadAllNalogaRequest;
import com.arctur.Podatek.ReadAllNalogaResponse;
import com.arctur.Podatek.ReadAllRequest;
import com.arctur.Podatek.ReadAllResponse;
import com.arctur.Podatek.ReadAllSestanekRequest;
import com.arctur.Podatek.ReadAllSestanekResponse;
import com.arctur.Podatek.ReadMejnikRequest;
import com.arctur.Podatek.ReadMejnikResponse;
import com.arctur.Podatek.ReadNalogaRequest;
import com.arctur.Podatek.ReadNalogaResponse;
import com.arctur.Podatek.ReadSestanekRequest;
import com.arctur.Podatek.ReadSestanekResponse;
import com.arctur.Podatek.ReadZadolzitevRequest;
import com.arctur.Podatek.ReadZadolzitevResponse;
import com.arctur.Podatek.Sestanek;
import com.arctur.Podatek.UpdateMejnikRequest;
import com.arctur.Podatek.UpdateMejnikResponse;
import com.arctur.Podatek.UpdateNalogaRequest;
import com.arctur.Podatek.UpdateNalogaResponse;
import com.arctur.Podatek.UpdateSestanekRequest;
import com.arctur.Podatek.UpdateSestanekResponse;
import com.arctur.Podatek.UpdateUreRequest;
import com.arctur.Podatek.UpdateUreResponse;
import com.arctur.Podatek.UpdateZadolzitevRequest;
import com.arctur.Podatek.UpdateZadolzitevResponse;
import com.arctur.Podatek.Zadolzitev;
import com.arctur.Podatek.Zaposlen;
import com.arctur.podatekGrpc.podatekImplBase;

import io.grpc.stub.StreamObserver;

public class PodatekService extends podatekImplBase{
	
	// JDBC driver name and database URL 
	   final String JDBC_DRIVER = "org.h2.Driver";   
	   final String DB_URL = "jdbc:h2:file:./target/baza";  
	   
	   //  Database credentials 
	   final String USER = "mark"; 
	   final String PASS = ""; 
	
	private Connection connect(){
		Connection conn=null;
		try{
			Class.forName(JDBC_DRIVER); 
	        
	        System.out.println("Connecting to database..."); 
	        
	        conn = DriverManager.getConnection(DB_URL,USER,PASS);  
	        
	        System.out.println("Connected database successfully..."); 
		}catch(SQLException se) { 
	        // Handle errors for JDBC 
	        se.printStackTrace(); 
	     } catch(Exception e) { 
	        // Handle errors for Class.forName 
	        e.printStackTrace(); 
	     }
		
		return conn;
	}
	
	
	@Override
	public void dodajZaposlenega(DodajZaposlenegaRequest request, StreamObserver<DodajZaposlenegaResponse> responseObserver){
		Connection povezava=connect();
		int id=0;
		if(povezava!=null){
			PreparedStatement stmt=null;
			try {
				if(request.getZaposlen().getIme()!="" && request.getZaposlen().getPriimek()!=""){
					stmt=povezava.prepareStatement("INSERT INTO ZAPOSLENI(IME,PRIIMEK) VALUES(?,?)",Statement.RETURN_GENERATED_KEYS);
					stmt.setString(1, request.getZaposlen().getIme());
					stmt.setString(2, request.getZaposlen().getPriimek());
					
					stmt.executeUpdate();
					ResultSet rs = stmt.getGeneratedKeys();
					while(rs.next()) { 
						id=rs.getInt(1);
			         } 
			         rs.close();
				}
				else{
					try { 
			            if(povezava!=null) povezava.close(); 
			         } catch(SQLException se) { 
			            se.printStackTrace(); 
			         }
					DodajZaposlenegaResponse response=DodajZaposlenegaResponse.newBuilder().setId(id).build();
			 		responseObserver.onNext(response);
			 		responseObserver.onCompleted();
			 		return;
				}
				
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try { 
	            if(stmt!=null) stmt.close();  
		     } catch(SQLException se2) { 
		    	 
		     }
	         try { 
	            if(povezava!=null) povezava.close(); 
	         } catch(SQLException se) { 
	            se.printStackTrace(); 
	         }
		}
		
		
		DodajZaposlenegaResponse response=DodajZaposlenegaResponse.newBuilder().setId(id).build();
 		responseObserver.onNext(response);
 		responseObserver.onCompleted();
	}
	
	private Zaposlen getZaposlen(int id){
		Connection povezava=connect();
		Zaposlen z=null;
		if(povezava!=null){
			PreparedStatement stmt=null;
			try {
				stmt=povezava.prepareStatement("SELECT * FROM ZAPOSLENI WHERE ID=?");
				stmt.setInt(1, id);
				ResultSet rs=stmt.executeQuery();
				while(rs.next()){
					z=Zaposlen.newBuilder().setId(rs.getInt(1)).setIme(rs.getString(2)).setPriimek(rs.getString(3)).build();
				}
				
			} catch (SQLException e) {
				System.out.println("Oseba s tem id-jem ne obstaja!");
				e.printStackTrace();
				
			}
			try { 
	            if(stmt!=null) stmt.close();  
		     } catch(SQLException se2) { 
		    	 
		     }
	         try { 
	            if(povezava!=null) povezava.close(); 
	         } catch(SQLException se) { 
	            se.printStackTrace(); 
	         }
		}
		return z;
	}
	
	@Override
	public void pridobiZaposlenega(PridobiZaposlenegaRequest request, StreamObserver<PridobiZaposlenegaResponse> responseObserver){
		Zaposlen z=getZaposlen((int)request.getId());
		if(z==null){
			PridobiZaposlenegaResponse response=PridobiZaposlenegaResponse.newBuilder().build();
	 		responseObserver.onNext(response);
	 		responseObserver.onCompleted();
		}else{
			PridobiZaposlenegaResponse response=PridobiZaposlenegaResponse.newBuilder().setZaposlen(z).build();
	 		responseObserver.onNext(response);
	 		responseObserver.onCompleted();
		}
		
	}
	
	@Override
	public void odstraniZaposlenega(OdstraniZaposlenegaRequest request, StreamObserver<OdstraniZaposlenegaResponse> responseObserver){
		Connection povezava=connect();
		Zaposlen z=null;
		if(povezava!=null){
			PreparedStatement stmt=null;
			try {
				odstraniZadolzitevByOseba((int)request.getId(),1);
				odstraniZadolzitevByOseba((int)request.getId(),2);
				odstraniZadolzitevByOseba((int)request.getId(),3);
				
				stmt=povezava.prepareStatement("DELETE FROM ZAPOSLENI WHERE ID=?");
				stmt.setInt(1, (int)request.getId());
				stmt.executeUpdate();
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try { 
	            if(stmt!=null) stmt.close();  
		     } catch(SQLException se2) { 
		    	 
		     }
	         try { 
	            if(povezava!=null) povezava.close(); 
	         } catch(SQLException se) { 
	            se.printStackTrace(); 
	         }
		}
		OdstraniZaposlenegaResponse response=OdstraniZaposlenegaResponse.newBuilder().setId(1).build();
 		responseObserver.onNext(response);
 		responseObserver.onCompleted();
	}
	
	private void insertZadolzitev(int idZadolzitve, int kateri, List<Zadolzitev> list){
		String beseda="";
		switch(kateri){
			case 1:
				beseda="ZADOLZITEVN";
				break;
			case 2:
				beseda="ZADOLZITEVS";
				break;
			case 3:
				beseda="ZADOLZITEVM";
				break;
		}
		String sql="INSERT INTO "+beseda+"(IDZADOLZITVE,IDZAPOSLENEGA,OPRAVLJENEURE,DODELJENEURE) VALUES(?,?,?,?)";
		Connection povezava=connect();
		if(povezava!=null){
			PreparedStatement stmt=null;
			try {
				stmt=povezava.prepareStatement(sql);
				Zadolzitev z=null;
				for(int i=0; i<list.size(); i++){
					z=list.get(i);
					if(getZaposlen((int)z.getIdZaposlenega())!=null){
						stmt.setInt(1, idZadolzitve);
						stmt.setInt(2, (int) z.getIdZaposlenega());
						stmt.setInt(3,(int) z.getOpravljeneUre());
						stmt.setInt(4,(int) z.getDodeljeneUre());
						stmt.executeUpdate();
					}
					else{
						System.out.println("Zaposlen z id="+z.getIdZaposlenega()+" ne obstaja!" );
					}
					
					
				}
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try { 
	            if(stmt!=null) stmt.close();  
		     } catch(SQLException se2) { 
		    	 
		     }
	         try { 
	            if(povezava!=null) povezava.close(); 
	         } catch(SQLException se) { 
	            se.printStackTrace(); 
	         }
		}
	}
	
	private List<Zadolzitev> getZadolzitev(int idZadolzitve, int kateri){
		List<Zadolzitev> list = new ArrayList<Zadolzitev>();
		String beseda="";
		switch(kateri){
			case 1:
				beseda="ZADOLZITEVN";
				break;
			case 2:
				beseda="ZADOLZITEVS";
				break;
			case 3:
				beseda="ZADOLZITEVM";
				break;
		}
		String sql="SELECT * FROM "+beseda+" WHERE IDZADOLZITVE=?";
		Connection povezava=connect();
		if(povezava!=null){
			PreparedStatement stmt=null;
			try {
				stmt=povezava.prepareStatement(sql);
				stmt.setInt(1, idZadolzitve);
				ResultSet zadolzeni=stmt.executeQuery();
				while(zadolzeni.next()){
					Zadolzitev z=Zadolzitev.newBuilder().setIdZaposlenega(zadolzeni.getInt(2)).setOpravljeneUre(zadolzeni.getInt(3)).setDodeljeneUre(zadolzeni.getInt(4)).build();
					list.add(z);
				}
				zadolzeni.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try { 
	            if(stmt!=null) stmt.close();  
		     } catch(SQLException se2) { 
		    	 
		     }
	         try { 
	            if(povezava!=null) povezava.close(); 
	         } catch(SQLException se) { 
	            se.printStackTrace(); 
	         }
		}
		return list;
	}
	
	
	@Override
	public void createZadolzitev(CreateZadolzitevRequest request, StreamObserver<CreateZadolzitevResponse> responseObserver){
		insertZadolzitev((int)request.getIdZadolzitve(),(int)request.getKateri(),request.getZadolzeniList());
		CreateZadolzitevResponse response=CreateZadolzitevResponse.newBuilder().setId(1).build();
 		responseObserver.onNext(response);
 		responseObserver.onCompleted();
	}
	
	@Override
	public void readZadolzitev(ReadZadolzitevRequest request, StreamObserver<ReadZadolzitevResponse> responseObserver){
		List<Zadolzitev> list=getZadolzitev((int)request.getIdZadolzitve(),(int)request.getKateri());
		ReadZadolzitevResponse response=ReadZadolzitevResponse.newBuilder().addAllZadolzitev(list).build();
 		responseObserver.onNext(response);
 		responseObserver.onCompleted();
	}
	
	@Override
	public void updateZadolzitev(UpdateZadolzitevRequest request, StreamObserver<UpdateZadolzitevResponse> responseObserver){
		String beseda="";
		switch((int)request.getKateri()){
			case 1:
				beseda="ZADOLZITEVN";
				break;
			case 2:
				beseda="ZADOLZITEVS";
				break;
			case 3:
				beseda="ZADOLZITEVM";
				break;
		}
		String sql="UPDATE "+beseda+" SET OPRAVLJENEURE=?,DODELJENEURE=? WHERE IDZADOLZITVE=? AND IDZAPOSLENEGA=?";
		Connection povezava=connect();
		List<Zadolzitev> list=request.getZadolzeniList();
		if(povezava!=null){
			PreparedStatement stmt=null;
			try {
				stmt=povezava.prepareStatement(sql);
				Zadolzitev z=null;
				for(int i=0; i<list.size(); i++){
					z=list.get(i);
					stmt.setInt(1,(int) z.getOpravljeneUre());
					stmt.setInt(2,(int) z.getDodeljeneUre());
					stmt.setInt(3, (int)request.getIdZadolzitve());
					stmt.setInt(4, (int) z.getIdZaposlenega());
					stmt.executeUpdate();
					
				}
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try { 
	            if(stmt!=null) stmt.close();  
		     } catch(SQLException se2) { 
		    	 
		     }
	         try { 
	            if(povezava!=null) povezava.close(); 
	         } catch(SQLException se) { 
	            se.printStackTrace(); 
	         }
		}
		UpdateZadolzitevResponse response=UpdateZadolzitevResponse.newBuilder().setId(1).build();
 		responseObserver.onNext(response);
 		responseObserver.onCompleted();
	}
	
	private void odstraniZadolzitev(int idZadolzitve, int kateri){
		String beseda="";
		switch(kateri){
			case 1:
				beseda="ZADOLZITEVN";
				break;
			case 2:
				beseda="ZADOLZITEVS";
				break;
			case 3:
				beseda="ZADOLZITEVM";
				break;
		}
		String sql="DELETE FROM "+beseda+" WHERE IDZADOLZITVE=?";
		Connection povezava=connect();
		if(povezava!=null){
			PreparedStatement stmt=null;
			try {
				stmt=povezava.prepareStatement(sql);
				stmt.setInt(1, idZadolzitve);
				stmt.executeUpdate();
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try { 
	            if(stmt!=null) stmt.close();    
	            if(povezava!=null) povezava.close();
		     } catch(SQLException se2) { 
		    	 
		     }
		}
	}
	
	@Override
	public void deleteZadolzitev(DeleteZadolzitevRequest request, StreamObserver<DeleteZadolzitevResponse> responseObserver){
		odstraniZadolzitev((int)request.getIdZadolzitve(),(int)request.getKateri());
		DeleteZadolzitevResponse response=DeleteZadolzitevResponse.newBuilder().setId(1).build();
 		responseObserver.onNext(response);
 		responseObserver.onCompleted();
	}
	
	private void odstraniZadolzitevByOseba(int id,int kateri){
		String beseda="";
		switch(kateri){
			case 1:
				beseda="ZADOLZITEVN";
				break;
			case 2:
				beseda="ZADOLZITEVS";
				break;
			case 3:
				beseda="ZADOLZITEVM";
				break;
		}
		String sql="DELETE FROM "+beseda+" WHERE IDZAPOSLENEGA=?";
		Connection povezava=connect();
		if(povezava!=null){
			PreparedStatement stmt=null;
			try {
				stmt=povezava.prepareStatement(sql);
				stmt.setInt(1, id);
				stmt.executeUpdate();
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try { 
	            if(stmt!=null) stmt.close();    
	            if(povezava!=null) povezava.close();
		     } catch(SQLException se2) { 
		    	 
		     }
		}
	}
	
	@Override
	public void deleteZadolzitevByOseba(DeleteZadolzitevByOsebaRequest request, StreamObserver<DeleteZadolzitevByOsebaResponse> responseObserver){
		String beseda="";
		switch((int)request.getKateri()){
			case 1:
				beseda="ZADOLZITEVN";
				break;
			case 2:
				beseda="ZADOLZITEVS";
				break;
			case 3:
				beseda="ZADOLZITEVM";
				break;
		}
		String sql="DELETE FROM "+beseda+" WHERE IDZADOLZITVE=? AND IDZAPOSLENEGA=?";
		Connection povezava=connect();
		if(povezava!=null){
			PreparedStatement stmt=null;
			try {
				stmt=povezava.prepareStatement(sql);
				stmt.setInt(1,(int) request.getIdZadolzitve());
				stmt.setInt(2,(int) request.getIdZaposlenega());
				stmt.executeUpdate();
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try { 
	            if(stmt!=null) stmt.close();    
	            if(povezava!=null) povezava.close();
		     } catch(SQLException se2) { 
		    	 
		     }
		}
		DeleteZadolzitevByOsebaResponse response=DeleteZadolzitevByOsebaResponse.newBuilder().setId(1).build();
 		responseObserver.onNext(response);
 		responseObserver.onCompleted();
	}
	
	@Override
	public void updateUre(UpdateUreRequest request, StreamObserver<UpdateUreResponse> responseObserver){
		String beseda="";
		int id=0;
		switch((int)request.getKateri()){
			case 1:
				beseda="ZADOLZITEVN";
				break;
			case 2:
				beseda="ZADOLZITEVS";
				break;
			case 3:
				beseda="ZADOLZITEVM";
				break;
		}
		String sql="UPDATE "+beseda+" SET OPRAVLJENEURE=? WHERE IDZADOLZITVE=? AND IDZAPOSLENEGA=?";
		Connection povezava=connect();
		if(povezava!=null){
			PreparedStatement stmt=null;
			try {
				stmt=povezava.prepareStatement(sql);
				stmt.setInt(1,(int) request.getOpravljeneUre());
				stmt.setInt(2, (int)request.getIdZadolzitve());
				stmt.setInt(3, (int) request.getIdZaposlenega());
				stmt.executeUpdate();
				id=1;
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try { 
	            if(stmt!=null) stmt.close();  
		     } catch(SQLException se2) { 
		    	 
		     }
	         try { 
	            if(povezava!=null) povezava.close(); 
	         } catch(SQLException se) { 
	            se.printStackTrace(); 
	         }
		}
		UpdateUreResponse response=UpdateUreResponse.newBuilder().setId(id).build();
 		responseObserver.onNext(response);
 		responseObserver.onCompleted();
	}
	
	@Override
	public void createSestanek(CreateSestanekRequest request, StreamObserver<CreateSestanekResponse> responseObserver){
		Connection povezava=connect();
		int id=0;
		if(povezava!=null){
			PreparedStatement stmt=null;
			try {
				stmt = povezava.prepareStatement("INSERT into Sestanek(NAZIV,DATUM,URA_ZACETKA,URA_ZAKLJUCKA) VALUES(?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
				if(request.getSestanek().getNaziv()!=""){
					stmt.setString(1, request.getSestanek().getNaziv());
					LocalDate d=LocalDate.of(request.getSestanek().getDatum().getYear(),request.getSestanek().getDatum().getMonth(), request.getSestanek().getDatum().getDay());
					Date datum=Date.valueOf(d);
					stmt.setDate(2, datum);
					stmt.setInt(3, request.getSestanek().getUraZacetka());
					stmt.setInt(4, request.getSestanek().getUraZakljucka());
				}
				else{
					 try { 
				            if(povezava!=null) povezava.close(); 
				         } catch(SQLException se) { 
				            se.printStackTrace(); 
				         }
					CreateSestanekResponse response=CreateSestanekResponse.newBuilder().setId(0).build();
			 		responseObserver.onNext(response);
			 		responseObserver.onCompleted();
			 		return;
				}	
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				stmt.executeUpdate();
				ResultSet rs = stmt.getGeneratedKeys();
				while(rs.next()) { 
					id=rs.getInt(1);
					insertZadolzitev(id, 2, request.getSestanek().getZadolzeniList());
		         } 
		         rs.close();
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
	         try { 
		            if(stmt!=null) stmt.close();  
		            if(povezava!=null) povezava.close(); 
		     } catch(SQLException se2) { 
		    	 
		     }
	         
	         CreateSestanekResponse response=CreateSestanekResponse.newBuilder().setId(id).build();
	 		responseObserver.onNext(response);
	 		responseObserver.onCompleted();
		}
	}
	
	@Override
	public void readSestanek(ReadSestanekRequest request, StreamObserver<ReadSestanekResponse> responseObserver){
		Connection povezava=connect();
		if(povezava!=null){
			PreparedStatement stmt=null;
			try {
				stmt=povezava.prepareStatement("SELECT * FROM SESTANEK WHERE ID=?");
				stmt.setInt(1, (int) request.getId());
				ResultSet r=stmt.executeQuery();
				
				List<Zadolzitev> list=getZadolzitev((int) request.getId(),2);
				
				Sestanek s=null;
				Datum datum=null;
				while(r.next()){
					datum=Datum.newBuilder().setYear(r.getDate(3).toLocalDate().getYear()).setMonth(r.getDate(3).toLocalDate().getMonthValue()).setDay(r.getDate(3).toLocalDate().getDayOfMonth()).build();
					s=Sestanek.newBuilder().setId(r.getInt(1)).setNaziv(r.getString(2)).setDatum(datum).setUraZacetka(r.getInt(4)).setUraZakljucka(r.getInt(5)).addAllZadolzeni(list).build();
				}
				r.close();
				 try { 
			            if(stmt!=null) stmt.close();
			            if(povezava!=null) povezava.close();
			     } catch(SQLException se2) { 
			    	 
			     }
				 if(s==null){
					 ReadSestanekResponse response=ReadSestanekResponse.newBuilder().build();
				 		responseObserver.onNext(response);
				 		responseObserver.onCompleted();
				 }else{
					 ReadSestanekResponse response=ReadSestanekResponse.newBuilder().setSestanek(s).build();
				 		responseObserver.onNext(response);
				 		responseObserver.onCompleted();
				 }
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
	}
	
	@Override
	public void updateSestanek(UpdateSestanekRequest request, StreamObserver<UpdateSestanekResponse> responseObserver){
		Connection povezava=connect();
		if(povezava!=null){
			PreparedStatement stmt=null;
			try {
				LocalDate d=LocalDate.of(request.getSestanek().getDatum().getYear(),request.getSestanek().getDatum().getMonth(), request.getSestanek().getDatum().getDay());
				Date datum=Date.valueOf(d);
				if(request.getSestanek().getNaziv()!=""){
					stmt=povezava.prepareStatement("UPDATE SESTANEK SET NAZIV=?,DATUM=?,URA_ZACETKA=?,URA_ZAKLJUCKA=? WHERE ID=?");
					stmt.setString(1, request.getSestanek().getNaziv());
					stmt.setDate(2, datum);
					stmt.setInt(3,request.getSestanek().getUraZacetka());
					stmt.setInt(4, request.getSestanek().getUraZakljucka());
					stmt.setInt(5, (int) request.getSestanek().getId());
					stmt.executeUpdate();
				}
				else{
					try { 
			            if(povezava!=null) povezava.close(); 
			         } catch(SQLException se) { 
			            se.printStackTrace(); 
			         }
					UpdateSestanekResponse response=UpdateSestanekResponse.newBuilder().setId(0).build();
			 		responseObserver.onNext(response);
			 		responseObserver.onCompleted();
			 		return;
				}	
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try { 
	            if(stmt!=null) stmt.close();    
	            if(povezava!=null) povezava.close();
	     } catch(SQLException se2) { 
	    	 
	     }
			UpdateSestanekResponse response=UpdateSestanekResponse.newBuilder().setId(request.getSestanek().getId()).build();
	 		responseObserver.onNext(response);
	 		responseObserver.onCompleted();
			
		}
	}
	
	@Override
	public void deleteSestanek(DeleteSestanekRequest request, StreamObserver<DeleteSestanekResponse> responseObserver){
		Connection povezava=connect();
		if(povezava!=null){
			PreparedStatement stmt=null;
			try {
				odstraniZadolzitev((int) request.getId(),2);
				
				stmt=povezava.prepareStatement("DELETE FROM SESTANEK WHERE ID=?");
				stmt.setInt(1, (int) request.getId());
				stmt.executeUpdate();
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try { 
	            if(stmt!=null) stmt.close();    
	            if(povezava!=null) povezava.close();
		     } catch(SQLException se2) { 
		    	 
		     }
			DeleteSestanekResponse response=DeleteSestanekResponse.newBuilder().setId(1).build();
	 		responseObserver.onNext(response);
	 		responseObserver.onCompleted();
		}
	}
	
	@Override
	public void readAllSestanek(ReadAllSestanekRequest request, StreamObserver<ReadAllSestanekResponse> responseObserver){
		Connection povezava=connect();
		if(povezava!=null){
			PreparedStatement stmt=null;
			List<Sestanek> list=new ArrayList<Sestanek>();
			try {
				stmt=povezava.prepareStatement("SELECT * FROM SESTANEK");
				ResultSet rs=stmt.executeQuery();
				Sestanek s=null;
				Datum datum=null;
				while(rs.next()){
					datum=Datum.newBuilder().setYear(rs.getDate(3).toLocalDate().getYear()).setMonth(rs.getDate(3).toLocalDate().getMonthValue()).setDay(rs.getDate(3).toLocalDate().getDayOfMonth()).build();
					
					List<Zadolzitev> zadolzeni=getZadolzitev(rs.getInt(1),2);
					s=Sestanek.newBuilder().setId(rs.getInt(1)).setNaziv(rs.getString(2)).setDatum(datum).setUraZacetka(rs.getInt(4)).setUraZakljucka(rs.getInt(5)).addAllZadolzeni(zadolzeni).build();
					list.add(s);
				}
				rs.close();
				
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try { 
	            if(stmt!=null) stmt.close();
	            if(povezava!=null) povezava.close();
		     } catch(SQLException se2) { 
		    	 
		     }
			ReadAllSestanekResponse response=ReadAllSestanekResponse.newBuilder().addAllSestanek(list).build();
	 		responseObserver.onNext(response);
	 		responseObserver.onCompleted();
		}
	}
	
	@Override
	public void createNaloga(CreateNalogaRequest request, StreamObserver<CreateNalogaResponse> responseObserver){
		Connection povezava=connect();
		if(povezava!=null){
			PreparedStatement stmt=null;
			int id=0;
			try {
				if(request.getNaloga().getNaziv()!=""){
					stmt=povezava.prepareStatement("INSERT INTO NALOGA(NAZIV,DATUM_ZACETKA,DATUM_ZAKLJUCKA) VALUES(?,?,?)",Statement.RETURN_GENERATED_KEYS);
					stmt.setString(1, request.getNaloga().getNaziv());
					LocalDate d1=LocalDate.of(request.getNaloga().getDatumZacetka().getYear(),request.getNaloga().getDatumZacetka().getMonth(), request.getNaloga().getDatumZacetka().getDay());
					Date datum1=Date.valueOf(d1);
					LocalDate d2=LocalDate.of(request.getNaloga().getDatumZakljucka().getYear(),request.getNaloga().getDatumZakljucka().getMonth(), request.getNaloga().getDatumZakljucka().getDay());
					Date datum2=Date.valueOf(d2);
					stmt.setDate(2, datum1);
					stmt.setDate(3, datum2);
					stmt.executeUpdate();
					ResultSet rs = stmt.getGeneratedKeys();
					while(rs.next()) { 
						id=rs.getInt(1);
						insertZadolzitev(id,1,request.getNaloga().getZadolzeniList());
			         } 
			         rs.close();
				}else{
					try {     
			            if(povezava!=null) povezava.close();
				     } catch(SQLException se2) { 
				    	 
				     }
					CreateNalogaResponse response=CreateNalogaResponse.newBuilder().setId(id).build();
			 		responseObserver.onNext(response);
			 		responseObserver.onCompleted();
			 		return;
				}
				
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
			try { 
	            if(stmt!=null) stmt.close();    
	            if(povezava!=null) povezava.close();
		     } catch(SQLException se2) { 
		    	 
		     }
			CreateNalogaResponse response=CreateNalogaResponse.newBuilder().setId(id).build();
	 		responseObserver.onNext(response);
	 		responseObserver.onCompleted();
		}
	}
	
	@Override
	public void readNaloga(ReadNalogaRequest request, StreamObserver<ReadNalogaResponse> responseObserver){
		Connection povezava=connect();
		if(povezava!=null){
			PreparedStatement stmt=null;
			try {
				stmt=povezava.prepareStatement("SELECT * FROM NALOGA WHERE ID=?");
				stmt.setInt(1, (int) request.getId());
				ResultSet r=stmt.executeQuery();
				
				List<Zadolzitev> list=getZadolzitev((int) request.getId(),1);

				Naloga n=null;
				Datum datum1=null;
				Datum datum2=null;
				while(r.next()){
					datum1=Datum.newBuilder().setYear(r.getDate(3).toLocalDate().getYear()).setMonth(r.getDate(3).toLocalDate().getMonthValue()).setDay(r.getDate(3).toLocalDate().getDayOfMonth()).build();
					datum2=Datum.newBuilder().setYear(r.getDate(4).toLocalDate().getYear()).setMonth(r.getDate(4).toLocalDate().getMonthValue()).setDay(r.getDate(4).toLocalDate().getDayOfMonth()).build();

					n=Naloga.newBuilder().setId(r.getInt(1)).setNaziv(r.getString(2)).setDatumZacetka(datum1).setDatumZakljucka(datum2).addAllZadolzeni(list).build();
				}
				r.close();
				 try { 
			            if(stmt!=null) stmt.close();  
			            if(povezava!=null) povezava.close();
			     } catch(SQLException se2) { 
			    	 
			     }
				 if(n==null){
					 ReadNalogaResponse response=ReadNalogaResponse.newBuilder().build();
				 		responseObserver.onNext(response);
				 		responseObserver.onCompleted();
				 }else{
					 ReadNalogaResponse response=ReadNalogaResponse.newBuilder().setNaloga(n).build();
				 		responseObserver.onNext(response);
				 		responseObserver.onCompleted();
				 }
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
	}
	
	@Override
	public void updateNaloga(UpdateNalogaRequest request, StreamObserver<UpdateNalogaResponse> responseObserver){
		Connection povezava=connect();
		if(povezava!=null){
			PreparedStatement stmt=null;
			try {
				if(request.getNaloga().getNaziv()!=""){
					LocalDate d1=LocalDate.of(request.getNaloga().getDatumZacetka().getYear(),request.getNaloga().getDatumZacetka().getMonth(), request.getNaloga().getDatumZacetka().getDay());
					Date datum1=Date.valueOf(d1);
					LocalDate d2=LocalDate.of(request.getNaloga().getDatumZakljucka().getYear(),request.getNaloga().getDatumZakljucka().getMonth(), request.getNaloga().getDatumZakljucka().getDay());
					Date datum2=Date.valueOf(d2);
					stmt=povezava.prepareStatement("UPDATE NALOGA SET NAZIV=?,DATUM_ZACETKA=?,DATUM_ZAKLJUCKA=? WHERE ID=?");
					stmt.setString(1, request.getNaloga().getNaziv());
					stmt.setDate(2, datum1);
					stmt.setDate(3, datum2);
					stmt.setInt(4, (int) request.getNaloga().getId());
					stmt.executeUpdate();
				}else{
					try {  
			            if(povezava!=null) povezava.close();
				     } catch(SQLException se2) { 
				    	 
				     }
					UpdateNalogaResponse response=UpdateNalogaResponse.newBuilder().setId(0).build();
			 		responseObserver.onNext(response);
			 		responseObserver.onCompleted();
			 		return;
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try { 
	            if(stmt!=null) stmt.close();    
	            if(povezava!=null) povezava.close();
	     } catch(SQLException se2) { 
	    	 
	     }
			UpdateNalogaResponse response=UpdateNalogaResponse.newBuilder().setId(request.getNaloga().getId()).build();
	 		responseObserver.onNext(response);
	 		responseObserver.onCompleted();
			
		}
	}
	
	@Override
	public void deleteNaloga(DeleteNalogaRequest request, StreamObserver<DeleteNalogaResponse> responseObserver){
		Connection povezava=connect();
		int id=0;
		if(povezava!=null){
			PreparedStatement stmt=null;
			try {
				odstraniZadolzitev((int) request.getId(),1);
				
				stmt=povezava.prepareStatement("DELETE FROM NALOGA WHERE ID=?");
				stmt.setInt(1, (int) request.getId());
				stmt.executeUpdate();
				id=1;
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try { 
	            if(stmt!=null) stmt.close();    
	            if(povezava!=null) povezava.close();
		     } catch(SQLException se2) { 
		    	 
		     }
			DeleteNalogaResponse response=DeleteNalogaResponse.newBuilder().setId(id).build();
	 		responseObserver.onNext(response);
	 		responseObserver.onCompleted();
		}
	}
	
	@Override
	public void readAllNaloga(ReadAllNalogaRequest request, StreamObserver<ReadAllNalogaResponse> responseObserver){
		Connection povezava=connect();
		if(povezava!=null){
			PreparedStatement stmt=null;
			List<Naloga> list=new ArrayList<Naloga>();
			try {
				stmt=povezava.prepareStatement("SELECT * FROM NALOGA");
				ResultSet rs=stmt.executeQuery();
				Naloga n=null;
				Datum datum1=null;
				Datum datum2=null;
				while(rs.next()){
					datum1=Datum.newBuilder().setYear(rs.getDate(3).toLocalDate().getYear()).setMonth(rs.getDate(3).toLocalDate().getMonthValue()).setDay(rs.getDate(3).toLocalDate().getDayOfMonth()).build();
					datum2=Datum.newBuilder().setYear(rs.getDate(4).toLocalDate().getYear()).setMonth(rs.getDate(4).toLocalDate().getMonthValue()).setDay(rs.getDate(4).toLocalDate().getDayOfMonth()).build();
					
					List<Zadolzitev> zadolzeni=getZadolzitev( rs.getInt(1),1);

					n=Naloga.newBuilder().setId(rs.getInt(1)).setNaziv(rs.getString(2)).setDatumZacetka(datum1).setDatumZakljucka(datum2).addAllZadolzeni(zadolzeni).build();
					list.add(n);
				}
				rs.close();
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try { 
	            if(stmt!=null) stmt.close();
	            if(povezava!=null) povezava.close();
		     } catch(SQLException se2) { 
		    	 
		     }
			ReadAllNalogaResponse response=ReadAllNalogaResponse.newBuilder().addAllNaloga(list).build();
	 		responseObserver.onNext(response);
	 		responseObserver.onCompleted();
		}
	}
	
	@Override
	public void createMejnik(CreateMejnikRequest request, StreamObserver<CreateMejnikResponse> responseObserver){
		Connection povezava=connect();
		int id=0;
		if(povezava!=null){
			PreparedStatement stmt=null;
			PreparedStatement stmt2=null;
			try {
				if(request.getMejnik().getNaziv()!=""){
					stmt=povezava.prepareStatement("INSERT INTO Mejnik(NAZIV,DATUM_ZAKLJUCKA) VALUES(?,?)",Statement.RETURN_GENERATED_KEYS);
					stmt.setString(1, request.getMejnik().getNaziv());
					LocalDate d=LocalDate.of(request.getMejnik().getDatumZakljucka().getYear(),request.getMejnik().getDatumZakljucka().getMonth(), request.getMejnik().getDatumZakljucka().getDay());
					Date datum=Date.valueOf(d);
					stmt.setDate(2, datum);
					stmt.executeUpdate();
					ResultSet rs = stmt.getGeneratedKeys();
					while(rs.next()){
						id=rs.getInt(1);
					}
					rs.close();
					
					stmt2=povezava.prepareStatement("INSERT into ZadolzitevM(IDZADOLZITVE,IDZAPOSLENEGA,OPRAVLJENEURE,DODELJENEURE) VALUES(?,?,?,?)");
					stmt2.setInt(1, id);
					stmt2.setInt(2, (int) request.getMejnik().getZadolzen().getIdZaposlenega());
					stmt2.setInt(3, 0);
					stmt2.setInt(4, 0);
					stmt2.executeUpdate();
				}
				else{
					try { 
			            if(povezava!=null) povezava.close();
				     } catch(SQLException se2) { 
				    	 
				     }
					CreateMejnikResponse response=CreateMejnikResponse.newBuilder().setId(id).build();
			 		responseObserver.onNext(response);
			 		responseObserver.onCompleted();
			 		return;
				}
					
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try { 
	            if(stmt!=null) stmt.close();
	            if(stmt2!=null) stmt2.close(); 
	            if(povezava!=null) povezava.close();
		     } catch(SQLException se2) { 
		    	 
		     }
			CreateMejnikResponse response=CreateMejnikResponse.newBuilder().setId(id).build();
	 		responseObserver.onNext(response);
	 		responseObserver.onCompleted();
		}
	}
	
	@Override
	public void readMejnik(ReadMejnikRequest request, StreamObserver<ReadMejnikResponse> responseObserver){
		Connection povezava=connect();
		if(povezava!=null){
			PreparedStatement stmt=null;
			PreparedStatement stmt2=null;
			try {
				stmt=povezava.prepareStatement("SELECT * FROM MEJNIK WHERE ID=?");
				stmt.setInt(1, (int) request.getId());
				ResultSet r=stmt.executeQuery();
				
				stmt2=povezava.prepareStatement("SELECT * FROM ZADOLZITEVM WHERE IDZADOLZITVE=?");
				stmt2.setInt(1, (int) request.getId());
				ResultSet zadolzeni=stmt2.executeQuery();
				Zadolzitev z=null;
				
				while(zadolzeni.next()){
					z=Zadolzitev.newBuilder().setIdZaposlenega(zadolzeni.getInt(2)).setOpravljeneUre(zadolzeni.getInt(3)).setDodeljeneUre(zadolzeni.getInt(4)).build();
				}
				zadolzeni.close();
				
				Mejnik m=null;
				Datum datum=null;
				while(r.next()){
					datum=Datum.newBuilder().setYear(r.getDate(3).toLocalDate().getYear()).setMonth(r.getDate(3).toLocalDate().getMonthValue()).setDay(r.getDate(3).toLocalDate().getDayOfMonth()).build();
					m=Mejnik.newBuilder().setId(r.getInt(1)).setNaziv(r.getString(2)).setDatumZakljucka(datum).setZadolzen(z).build();	
				}
				r.close();
				 try { 
			            if(stmt!=null) stmt.close();  
			            if(stmt2!=null) stmt2.close();  
			            if(povezava!=null) povezava.close();
			     } catch(SQLException se2) { 
			    	 
			     }
				 if(m==null){
					 ReadMejnikResponse response=ReadMejnikResponse.newBuilder().build();
				 		responseObserver.onNext(response);
				 		responseObserver.onCompleted();
				 }
				 else{
					 ReadMejnikResponse response=ReadMejnikResponse.newBuilder().setMejnik(m).build();
				 		responseObserver.onNext(response);
				 		responseObserver.onCompleted();
				 }
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
	}
	
	@Override
	public void updateMejnik(UpdateMejnikRequest request, StreamObserver<UpdateMejnikResponse> responseObserver){
		Connection povezava=connect();
		if(povezava!=null){
			PreparedStatement stmt=null;
			try {
				if(request.getMejnik().getNaziv()!=""){
					LocalDate d=LocalDate.of(request.getMejnik().getDatumZakljucka().getYear(),request.getMejnik().getDatumZakljucka().getMonth(), request.getMejnik().getDatumZakljucka().getDay());
					Date datum=Date.valueOf(d);
					stmt=povezava.prepareStatement("UPDATE MEJNIK SET NAZIV=?,DATUM_ZAKLJUCKA=? WHERE ID=?");
					stmt.setString(1, request.getMejnik().getNaziv());
					stmt.setDate(2, datum);
					stmt.setInt(3, (int) request.getMejnik().getId());
					stmt.executeUpdate();
				}else{
					try { 
			            if(povezava!=null) povezava.close();
				     } catch(SQLException se2) { 
				    	 
				     }
					UpdateMejnikResponse response=UpdateMejnikResponse.newBuilder().setId(0).build();
			 		responseObserver.onNext(response);
			 		responseObserver.onCompleted();
			 		return;
				}	
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try { 
	            if(stmt!=null) stmt.close();    
	            if(povezava!=null) povezava.close();
	     } catch(SQLException se2) { 
	    	 
	     }
			UpdateMejnikResponse response=UpdateMejnikResponse.newBuilder().setId(request.getMejnik().getId()).build();
	 		responseObserver.onNext(response);
	 		responseObserver.onCompleted();
			
		}
	}
	
	@Override
	public void deleteMejnik(DeleteMejnikRequest request, StreamObserver<DeleteMejnikResponse> responseObserver){
		Connection povezava=connect();
		if(povezava!=null){
			PreparedStatement stmt=null;
			try {
				stmt=povezava.prepareStatement("DELETE FROM ZADOLZITEVM WHERE IDZADOLZITVE=?");
				stmt.setInt(1, (int) request.getId());
				stmt.executeUpdate();
				
				stmt=povezava.prepareStatement("DELETE FROM MEJNIK WHERE ID=?");
				stmt.setInt(1, (int) request.getId());
				stmt.executeUpdate();
				
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try { 
	            if(stmt!=null) stmt.close();    
	            if(povezava!=null) povezava.close();
		     } catch(SQLException se2) { 
		    	 
		     }
			DeleteMejnikResponse response=DeleteMejnikResponse.newBuilder().setId(1).build();
	 		responseObserver.onNext(response);
	 		responseObserver.onCompleted();
		}
	}
	
	@Override
	public void readAllMejnik(ReadAllMejnikRequest request, StreamObserver<ReadAllMejnikResponse> responseObserver){
		Connection povezava=connect();
		if(povezava!=null){
			PreparedStatement stmt=null;
			PreparedStatement stmt2=null;
			List<Mejnik> list=new ArrayList<Mejnik>();
			try {
				stmt=povezava.prepareStatement("SELECT * FROM MEJNIK");
				ResultSet rs=stmt.executeQuery();
				Mejnik m=null;
				Datum datum=null;
				while(rs.next()){
					datum=Datum.newBuilder().setYear(rs.getDate(3).toLocalDate().getYear()).setMonth(rs.getDate(3).toLocalDate().getMonthValue()).setDay(rs.getDate(3).toLocalDate().getDayOfMonth()).build();
					
					Zadolzitev z=null;
					stmt2=povezava.prepareStatement("SELECT * FROM ZADOLZITEVM WHERE IDZADOLZITVE=?");
					stmt2.setInt(1, rs.getInt(1));
					ResultSet r=stmt2.executeQuery();
					while(r.next()){
						z=Zadolzitev.newBuilder().setIdZaposlenega(r.getInt(2)).setOpravljeneUre(r.getInt(3)).setDodeljeneUre(r.getInt(4)).build();
					}
					m=Mejnik.newBuilder().setId(rs.getInt(1)).setNaziv(rs.getString(2)).setDatumZakljucka(datum).setZadolzen(z).build();
					list.add(m);
					r.close();
				}
				rs.close();
				
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try { 
	            if(stmt!=null) stmt.close();
	            if(stmt2!=null) stmt2.close();
	            if(povezava!=null) povezava.close();
		     } catch(SQLException se2) { 
		    	 
		     }
			ReadAllMejnikResponse response=ReadAllMejnikResponse.newBuilder().addAllMejnik(list).build();
	 		responseObserver.onNext(response);
	 		responseObserver.onCompleted();
		}
	}
	
	
	private List<Opravek> pridobiOpravke(String sql1,String sql2){
		Connection povezava=connect();
		int opravljene=0;
		int dodeljene=0;
		int id=0;
		String naziv="";
		List<Opravek> list=new ArrayList<Opravek>();
		if(povezava!=null){
			PreparedStatement stmt=null;
			PreparedStatement stmt2=null;
			try {
				stmt2=povezava.prepareStatement(sql1);
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			try {
				ResultSet r=stmt2.executeQuery();
				while(r.next()){
					id=r.getInt(1);
					naziv=r.getString(2);
					
					
					try {
						stmt=povezava.prepareStatement(sql2);
						stmt.setInt(1, id);
					} catch (SQLException e) {
						e.printStackTrace();
					}
					try {
						
						ResultSet rs = stmt.executeQuery();
						while(rs.next()) { 
							opravljene=rs.getInt(1);
							dodeljene=rs.getInt(2);
				         } 
				         rs.close();
						
					} catch (SQLException e) {
						e.printStackTrace();
					}
			         try { 
				            if(stmt!=null) stmt.close();  
				     } catch(SQLException se2) { 
				    	 
				     }
					
			         Opravek o=Opravek.newBuilder().setNaziv(naziv).setSkupajOpravljeneUre(opravljene).setSkupajDodeljeneUre(dodeljene).build();
			         list.add(o);
			         opravljene=0;
			         dodeljene=0;
				}
				r.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			try { 
	            if(stmt2!=null) stmt2.close();  
	     } catch(SQLException se2) { 
	    	 
	     }
         try { 
            if(povezava!=null) povezava.close(); 
         } catch(SQLException se) { 
            se.printStackTrace(); 
         }
         
		}
		return list;
	}
	
	@Override
	public void readAll(ReadAllRequest request, StreamObserver<ReadAllResponse> responseObserver){
		List<Opravek> seznam=new ArrayList<Opravek>();
		seznam.addAll(pridobiOpravke("Select ID,NAZIV from SESTANEK","SELECT SUM(OPRAVLJENEURE) ,SUM(DODELJENEURE) from ZADOLZITEVS where idzadolzitve=?"));
		seznam.addAll(pridobiOpravke("Select ID,NAZIV from NALOGA","SELECT SUM(OPRAVLJENEURE) ,SUM(DODELJENEURE) from ZADOLZITEVN where idzadolzitve=?"));
		seznam.addAll(pridobiOpravke("Select ID,NAZIV from MEJNIK","SELECT SUM(OPRAVLJENEURE) ,SUM(DODELJENEURE) from ZADOLZITEVM where idzadolzitve=?"));
        ReadAllResponse response=ReadAllResponse.newBuilder().addAllOpravek(seznam).build();
 		responseObserver.onNext(response);
 		responseObserver.onCompleted();
	}
	
	@Override
	public void readAllSortedByNaziv(ReadAllRequest request, StreamObserver<ReadAllResponse> responseObserver){
		List<Opravek> seznam=new ArrayList<Opravek>();
		seznam.addAll(pridobiOpravke("Select ID,NAZIV from SESTANEK","SELECT SUM(OPRAVLJENEURE) ,SUM(DODELJENEURE) from ZADOLZITEVS where idzadolzitve=?"));
		seznam.addAll(pridobiOpravke("Select ID,NAZIV from NALOGA","SELECT SUM(OPRAVLJENEURE) ,SUM(DODELJENEURE) from ZADOLZITEVN where idzadolzitve=?"));
		seznam.addAll(pridobiOpravke("Select ID,NAZIV from MEJNIK","SELECT SUM(OPRAVLJENEURE) ,SUM(DODELJENEURE) from ZADOLZITEVM where idzadolzitve=?"));
		Collections.sort(seznam, new SortByNaziv());
        ReadAllResponse response=ReadAllResponse.newBuilder().addAllOpravek(seznam).build();
 		responseObserver.onNext(response);
 		responseObserver.onCompleted();
	}
	
	@Override
	public void readAllSortedByOpravljene(ReadAllRequest request, StreamObserver<ReadAllResponse> responseObserver){
		List<Opravek> seznam=new ArrayList<Opravek>();
		seznam.addAll(pridobiOpravke("Select ID,NAZIV from SESTANEK","SELECT SUM(OPRAVLJENEURE) ,SUM(DODELJENEURE) from ZADOLZITEVS where idzadolzitve=?"));
		seznam.addAll(pridobiOpravke("Select ID,NAZIV from NALOGA","SELECT SUM(OPRAVLJENEURE) ,SUM(DODELJENEURE) from ZADOLZITEVN where idzadolzitve=?"));
		seznam.addAll(pridobiOpravke("Select ID,NAZIV from MEJNIK","SELECT SUM(OPRAVLJENEURE) ,SUM(DODELJENEURE) from ZADOLZITEVM where idzadolzitve=?"));
		Collections.sort(seznam, new SortByOpravljene());
        ReadAllResponse response=ReadAllResponse.newBuilder().addAllOpravek(seznam).build();
 		responseObserver.onNext(response);
 		responseObserver.onCompleted();
	}
	
	@Override
	public void readAllSortedByDodeljene(ReadAllRequest request, StreamObserver<ReadAllResponse> responseObserver){
		List<Opravek> seznam=new ArrayList<Opravek>();
		seznam.addAll(pridobiOpravke("Select ID,NAZIV from SESTANEK","SELECT SUM(OPRAVLJENEURE) ,SUM(DODELJENEURE) from ZADOLZITEVS where idzadolzitve=?"));
		seznam.addAll(pridobiOpravke("Select ID,NAZIV from NALOGA","SELECT SUM(OPRAVLJENEURE) ,SUM(DODELJENEURE) from ZADOLZITEVN where idzadolzitve=?"));
		seznam.addAll(pridobiOpravke("Select ID,NAZIV from MEJNIK","SELECT SUM(OPRAVLJENEURE) ,SUM(DODELJENEURE) from ZADOLZITEVM where idzadolzitve=?"));
		Collections.sort(seznam, new SortByDodeljene());
        ReadAllResponse response=ReadAllResponse.newBuilder().addAllOpravek(seznam).build();
 		responseObserver.onNext(response);
 		responseObserver.onCompleted();
	}
	
	private List<Opravek> filterByNaziv(String sql1,String naziv,String sql2){
		Connection povezava=connect();
		int opravljene=0;
		int dodeljene=0;
		int id=0;
		String ime="";
		List<Opravek> list=new ArrayList<Opravek>();
		if(povezava!=null){
			PreparedStatement stmt=null;
			PreparedStatement stmt2=null;
			try {
				stmt2=povezava.prepareStatement(sql1);
				stmt2.setString(1, naziv);
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			try {
				ResultSet r=stmt2.executeQuery();
				while(r.next()){
					id=r.getInt(1);
					ime=r.getString(2);
					
					
					try {
						stmt=povezava.prepareStatement(sql2);
						stmt.setInt(1, id);
					} catch (SQLException e) {
						e.printStackTrace();
					}
					try {
						
						ResultSet rs = stmt.executeQuery();
						while(rs.next()) { 
							opravljene=rs.getInt(1);
							dodeljene=rs.getInt(2);
				         } 
				         rs.close();
						
					} catch (SQLException e) {
						e.printStackTrace();
					}
			         try { 
				            if(stmt!=null) stmt.close();  
				     } catch(SQLException se2) { 
				    	 
				     }
					
			         Opravek o=Opravek.newBuilder().setNaziv(ime).setSkupajOpravljeneUre(opravljene).setSkupajDodeljeneUre(dodeljene).build();
			         list.add(o);
			         opravljene=0;
			         dodeljene=0;
				}
				r.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			try { 
	            if(stmt2!=null) stmt2.close();  
	     } catch(SQLException se2) { 
	    	 
	     }
         try { 
            if(povezava!=null) povezava.close(); 
         } catch(SQLException se) { 
            se.printStackTrace(); 
         }
         
		}
		return list;
	}
	
	@Override
	public void filterByNaziv(FilterByNazivRequest request, StreamObserver<FilterByNazivResponse> responseObserver){
		List<Opravek> seznam=new ArrayList<Opravek>();
		seznam.addAll(filterByNaziv("Select ID,NAZIV from SESTANEK WHERE NAZIV=?",request.getNaziv(),"SELECT SUM(OPRAVLJENEURE) ,SUM(DODELJENEURE) from ZADOLZITEVS where idzadolzitve=?"));
		seznam.addAll(filterByNaziv("Select ID,NAZIV from NALOGA WHERE NAZIV=?",request.getNaziv(),"SELECT SUM(OPRAVLJENEURE) ,SUM(DODELJENEURE) from ZADOLZITEVN where idzadolzitve=?"));
		seznam.addAll(filterByNaziv("Select ID,NAZIV from MEJNIK WHERE NAZIV=?",request.getNaziv(),"SELECT SUM(OPRAVLJENEURE) ,SUM(DODELJENEURE) from ZADOLZITEVM where idzadolzitve=?"));
        FilterByNazivResponse response=FilterByNazivResponse.newBuilder().addAllOpravek(seznam).build();
 		responseObserver.onNext(response);
 		responseObserver.onCompleted();
	}
	
	private List<Opravek> filter(String sql1,String sql2,int stevilo,int byOseba){
		Connection povezava=connect();
		int opravljene=0;
		int dodeljene=0;
		int id=0;
		String ime="";
		List<Opravek> list=new ArrayList<Opravek>();
		if(povezava!=null){
			PreparedStatement stmt=null;
			PreparedStatement stmt2=null;
			PreparedStatement stmt3=null;
			try {
				stmt2=povezava.prepareStatement(sql1);
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			try {
				
				if(byOseba==1){
					stmt3=povezava.prepareStatement("SELECT * FROM ZAPOSLENI WHERE ID=?");
					stmt3.setInt(1, stevilo);
					ResultSet r1 = stmt3.executeQuery();
					if(!r1.next()){
						System.out.println("OSEBA NE OBSTAJA!");
						return list;
					}
					
				}
				
				ResultSet r=stmt2.executeQuery();
				while(r.next()){
					id=r.getInt(1);
					ime=r.getString(2);
					
					
					try {
						stmt=povezava.prepareStatement(sql2);
						stmt.setInt(1, id);
						stmt.setInt(2, stevilo);
					} catch (SQLException e) {
						e.printStackTrace();
					}
					try {
						ResultSet rs = stmt.executeQuery();
						while(rs.next()) {
							opravljene=rs.getInt(1);
							dodeljene=rs.getInt(2);
							Opravek o=Opravek.newBuilder().setNaziv(ime).setSkupajOpravljeneUre(opravljene).setSkupajDodeljeneUre(dodeljene).build();
					        list.add(o);
				         } 
				         rs.close();
						
					} catch (SQLException e) {
						e.printStackTrace();
					}
			         try { 
				            if(stmt!=null) stmt.close();  
				     } catch(SQLException se2) { 
				    	 
				     }
			         opravljene=0;
			         dodeljene=0;
				}
				r.close();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
			try { 
	            if(stmt2!=null) stmt2.close();  
	     } catch(SQLException se2) { 
	    	 
	     }
         try { 
            if(povezava!=null) povezava.close(); 
         } catch(SQLException se) { 
            se.printStackTrace(); 
         }
         
		}
		return list;
	}
	
	@Override
	public void filterByOpravljene(FilterByOpravljeneRequest request, StreamObserver<FilterByOpravljeneResponse> responseObserver){
		List<Opravek> seznam=new ArrayList<Opravek>();
		seznam.addAll(filter("Select ID,NAZIV from SESTANEK","SELECT SUM(OPRAVLJENEURE),SUM(DODELJENEURE) from ZADOLZITEVS where idzadolzitve=? HAVING SUM(OPRAVLJENEURE)=?",(int)request.getOpravljene(),0));
		seznam.addAll(filter("Select ID,NAZIV from NALOGA","SELECT SUM(OPRAVLJENEURE) ,SUM(DODELJENEURE) from ZADOLZITEVN where idzadolzitve=? HAVING SUM(OPRAVLJENEURE)=?",(int)request.getOpravljene(),0));
		seznam.addAll(filter("Select ID,NAZIV from MEJNIK","SELECT SUM(OPRAVLJENEURE) ,SUM(DODELJENEURE) from ZADOLZITEVM where idzadolzitve=? HAVING SUM(OPRAVLJENEURE)=?",(int)request.getOpravljene(),0));
        FilterByOpravljeneResponse response=FilterByOpravljeneResponse.newBuilder().addAllOpravek(seznam).build();
 		responseObserver.onNext(response);
 		responseObserver.onCompleted();
	}
	
	@Override
	public void filterByDodeljene(FilterByDodeljeneRequest request, StreamObserver<FilterByDodeljeneResponse> responseObserver){
		List<Opravek> seznam=new ArrayList<Opravek>();
		seznam.addAll(filter("Select ID,NAZIV from SESTANEK","SELECT SUM(OPRAVLJENEURE),SUM(DODELJENEURE) from ZADOLZITEVS where idzadolzitve=? HAVING SUM(DODELJENEURE)=?",(int)request.getDodeljene(),0));
		seznam.addAll(filter("Select ID,NAZIV from NALOGA","SELECT SUM(OPRAVLJENEURE) ,SUM(DODELJENEURE) from ZADOLZITEVN where idzadolzitve=? HAVING SUM(DODELJENEURE)=?",(int)request.getDodeljene(),0));
		seznam.addAll(filter("Select ID,NAZIV from MEJNIK","SELECT SUM(OPRAVLJENEURE) ,SUM(DODELJENEURE) from ZADOLZITEVM where idzadolzitve=? HAVING SUM(DODELJENEURE)=?",(int)request.getDodeljene(),0));
        FilterByDodeljeneResponse response=FilterByDodeljeneResponse.newBuilder().addAllOpravek(seznam).build();
 		responseObserver.onNext(response);
 		responseObserver.onCompleted();
	}
	
	@Override
	public void filterByOseba(FilterByOsebaRequest request, StreamObserver<FilterByOsebaResponse> responseObserver){
		List<Opravek> seznam=new ArrayList<Opravek>();
		seznam.addAll(filter("Select ID,NAZIV from SESTANEK","SELECT SUM(OPRAVLJENEURE),SUM(DODELJENEURE) from ZADOLZITEVS where idzadolzitve=? AND idzaposlenega=?",(int)request.getId(),1));
		seznam.addAll(filter("Select ID,NAZIV from NALOGA","SELECT SUM(OPRAVLJENEURE) ,SUM(DODELJENEURE) from ZADOLZITEVN where idzadolzitve=? AND idzaposlenega=?",(int)request.getId(),1));
		seznam.addAll(filter("Select ID,NAZIV from MEJNIK","SELECT SUM(OPRAVLJENEURE) ,SUM(DODELJENEURE) from ZADOLZITEVM where idzadolzitve=? AND idzaposlenega=?",(int)request.getId(),1));
        FilterByOsebaResponse response=FilterByOsebaResponse.newBuilder().addAllOpravek(seznam).build();
 		responseObserver.onNext(response);
 		responseObserver.onCompleted();
	}
	
}
class SortByNaziv implements Comparator<Opravek>{
	@Override
	public int compare(Opravek o1, Opravek o2) {
		return o1.getNaziv().compareTo(o2.getNaziv());
	}
	
}
class SortByOpravljene implements Comparator<Opravek>{
	@Override
	public int compare(Opravek o1, Opravek o2) {
		int a=(int)o1.getSkupajOpravljeneUre();
		int b=(int)o2.getSkupajOpravljeneUre();
		return a-b;
	}
	
}
class SortByDodeljene implements Comparator<Opravek>{
	@Override
	public int compare(Opravek o1, Opravek o2) {
		int a=(int)o1.getSkupajDodeljeneUre();
		int b=(int)o2.getSkupajDodeljeneUre();
		return a-b;
	}
	
}
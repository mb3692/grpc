package grpcArctur;

import java.io.IOException;

import org.flywaydb.core.Flyway;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import grpcArctur.PodatekService;

public class GrpcServer {
	public static void main(String[] args){
		int port = 8080;
		Server server = ServerBuilder.forPort(port)
		        .addService(new PodatekService())
		        .build();
		        try {
					server.start();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
		    //logger.info("Server started, listening on " + port);
		   System.out.println("Server started");
		   
		   Flyway flyway = Flyway.configure().dataSource("jdbc:h2:file:./target/baza", "mark", "").load();
	       flyway.migrate();

		      try {
					server.awaitTermination();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
		   } 
		   
		
} 

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.arctur.Podatek.CreateMejnikRequest;
import com.arctur.Podatek.CreateMejnikResponse;
import com.arctur.Podatek.CreateNalogaRequest;
import com.arctur.Podatek.CreateNalogaResponse;
import com.arctur.Podatek.CreateSestanekRequest;
import com.arctur.Podatek.CreateSestanekResponse;
import com.arctur.Podatek.Datum;
import com.arctur.Podatek.DeleteNalogaRequest;
import com.arctur.Podatek.DeleteNalogaResponse;
import com.arctur.Podatek.DeleteSestanekRequest;
import com.arctur.Podatek.DeleteSestanekResponse;
import com.arctur.Podatek.DodajZaposlenegaRequest;
import com.arctur.Podatek.DodajZaposlenegaResponse;
import com.arctur.Podatek.FilterByNazivRequest;
import com.arctur.Podatek.FilterByNazivResponse;
import com.arctur.Podatek.FilterByOpravljeneRequest;
import com.arctur.Podatek.FilterByOpravljeneResponse;
import com.arctur.Podatek.FilterByOsebaRequest;
import com.arctur.Podatek.FilterByOsebaResponse;
import com.arctur.Podatek.Mejnik;
import com.arctur.Podatek.Naloga;
import com.arctur.Podatek.ReadAllNalogaRequest;
import com.arctur.Podatek.ReadAllNalogaResponse;
import com.arctur.Podatek.ReadAllRequest;
import com.arctur.Podatek.ReadAllResponse;
import com.arctur.Podatek.ReadAllSestanekRequest;
import com.arctur.Podatek.ReadAllSestanekResponse;
import com.arctur.Podatek.ReadNalogaRequest;
import com.arctur.Podatek.ReadNalogaResponse;
import com.arctur.Podatek.ReadSestanekRequest;
import com.arctur.Podatek.ReadSestanekResponse;
import com.arctur.Podatek.Sestanek;
import com.arctur.Podatek.UpdateNalogaRequest;
import com.arctur.Podatek.UpdateNalogaResponse;
import com.arctur.Podatek.UpdateSestanekRequest;
import com.arctur.Podatek.UpdateSestanekResponse;
import com.arctur.Podatek.UpdateUreRequest;
import com.arctur.Podatek.UpdateUreResponse;
import com.arctur.Podatek.Zadolzitev;
import com.arctur.Podatek.Zaposlen;
import com.arctur.podatekGrpc;
import com.arctur.podatekGrpc.podatekBlockingStub;
public class GrpcClient {
	
	public static void main(String[] args){
		//docker ip - 192.168.99.100
		String target = "localhost:8080";
		ManagedChannel channel = ManagedChannelBuilder.forTarget(target)
		        .usePlaintext()
		        .build();
		
		podatekBlockingStub stub=podatekGrpc.newBlockingStub(channel);
		
		//dodamo zaposlenega
		Zaposlen zaposlen=Zaposlen.newBuilder().setId(0).setIme("Mark").setPriimek("Besednjak").build();
		DodajZaposlenegaRequest request1=DodajZaposlenegaRequest.newBuilder().setZaposlen(zaposlen).build();
		DodajZaposlenegaResponse response1;
		try {
	      response1 = stub.dodajZaposlenega(request1);
	      if((int)response1.getId()>0){
	    	  System.out.println("Zaposlen uspešno dodan. Njegov id="+response1.getId());
	      }
	      else{
	    	  System.out.println("Prišlo je do napake pri vnosu zaposlenega!");
	      }
	    } catch (StatusRuntimeException e) {
	      return;
	    }
		
		//dodamo sestanek
	    Datum datum=Datum.newBuilder().setDay(4).setMonth(4).setYear(2020).build();
	    Zadolzitev z=Zadolzitev.newBuilder().setIdZaposlenega(1).setDodeljeneUre(4).setOpravljeneUre(0).build();
	    List<Zadolzitev> list=new ArrayList<Zadolzitev>();
	    list.add(z);
		Sestanek s=Sestanek.newBuilder().setId(1).setNaziv("Prvi sestanek").setDatum(datum).setUraZacetka(11).setUraZakljucka(15).addAllZadolzeni(list).build();
		CreateSestanekRequest request2 = CreateSestanekRequest.newBuilder().setSestanek(s).build();
	    CreateSestanekResponse response2;
	    try {
	      response2 = stub.createSestanek(request2);
	      if((int)response2.getId()>0){
	    	  System.out.println("Sestanek uspešno dodan. Njegov id="+response2.getId());
	      }
	      else{
	    	  System.out.println("Prišlo je do napake pri vnosu sestanka!");
	      }
	    } catch (StatusRuntimeException e) {
	      return;
	    }
	    
	    //dodamo nalogo
	    Datum datum1=Datum.newBuilder().setDay(12).setMonth(12).setYear(2020).build();
		Datum datum2=Datum.newBuilder().setDay(15).setMonth(12).setYear(2020).build();
		List<Zadolzitev> list1=new ArrayList<Zadolzitev>();
		Zadolzitev z1=Zadolzitev.newBuilder().setIdZaposlenega(1).setDodeljeneUre(6).setOpravljeneUre(2).build();
		list1.add(z1);
		Naloga n=Naloga.newBuilder().setId(1).setNaziv("Prva Naloga").setDatumZacetka(datum1).setDatumZakljucka(datum2).addAllZadolzeni(list1).build();
		CreateNalogaRequest request3=CreateNalogaRequest.newBuilder().setNaloga(n).build();
		
		CreateNalogaResponse response3;
		try {
	    	response3=stub.createNaloga(request3);
	    	if((int)response3.getId()>0){
		    	  System.out.println("Naloga uspešno dodana. Njen id="+response3.getId());
		      }
		      else{
		    	  System.out.println("Prišlo je do napake pri vnosu naloge!");
		      }
		} catch (StatusRuntimeException e) {
		    return;
		}
		
		//popravimo nalogo
		Naloga n1=Naloga.newBuilder().setId(1).setNaziv("Prva Naloga Popravljena").setDatumZacetka(datum1).setDatumZakljucka(datum2).build();
		UpdateNalogaRequest request4=UpdateNalogaRequest.newBuilder().setNaloga(n1).build();
		UpdateNalogaResponse response4;
		try {
	    	response4=stub.updateNaloga(request4);
	    	if((int)response4.getId()>0){
		    	  System.out.println("Naloga uspešno popravljena. Njen id="+response4.getId());
		      }
		      else{
		    	  System.out.println("Prišlo je do napake pri popravljanju naloge!");
		      }
		} catch (StatusRuntimeException e) {
		    return;
		}
		
		//dodamo mejnik
		Zadolzitev z2=Zadolzitev.newBuilder().setIdZaposlenega(1).setDodeljeneUre(0).setOpravljeneUre(0).build();
		Mejnik m=Mejnik.newBuilder().setId(1).setNaziv("Prvi Mejnik").setDatumZakljucka(datum1).setZadolzen(z2).build();
		CreateMejnikRequest request5=CreateMejnikRequest.newBuilder().setMejnik(m).build();
		CreateMejnikResponse response5;
		try {
	    	response5=stub.createMejnik(request5);
	    	if((int)response5.getId()>0){
		    	  System.out.println("Mejnik uspešno dodan. Njegov id="+response5.getId());
		      }
		      else{
		    	  System.out.println("Prišlo je do napake pri vnosu mejnika!");
		      }
		} catch (StatusRuntimeException e) {
		    return;
		}
		
		//popravimo opravljene ure na nalogi
		UpdateUreRequest request6=UpdateUreRequest.newBuilder().setKateri(1).setIdZadolzitve(1).setIdZaposlenega(1).setOpravljeneUre(4).build();
		UpdateUreResponse response6;
		try {
	    	response6=stub.updateUre(request6);
	    	if((int)response6.getId()>0){
		    	  System.out.println("Ure uspešno posodobljene!");
		      }
		      else{
		    	  System.out.println("Prišlo je do napake pri popravljanju ur!");
		      }
		} catch (StatusRuntimeException e) {
		    return;
		}
		
		//pridobimo seznam vseh opravil
		ReadAllRequest request7=ReadAllRequest.newBuilder().build();
		ReadAllResponse response7;
		try {
	    	response7=stub.readAll(request7);
	    	System.out.println(response7.getOpravekList());
		} catch (StatusRuntimeException e) {
		    return;
		}
		
		//odstranimo nalogo
		DeleteNalogaRequest request8=DeleteNalogaRequest.newBuilder().setId(1).build();
		DeleteNalogaResponse response8;
		try {
	    	response8=stub.deleteNaloga(request8);
	    	if((int)response8.getId()>0){
		    	  System.out.println("Naloga uspešno odstranjena!");
		      }
		      else{
		    	  System.out.println("Prišlo je do napake pri brisanju naloge!");
		      }
		} catch (StatusRuntimeException e) {
		    return;
		}
		
		//ponovno pridobimo seznam vseh opravil sortiranih po opravljenih urah
		ReadAllRequest request9=ReadAllRequest.newBuilder().build();
		ReadAllResponse response9;
		try {
	    	response9=stub.readAllSortedByOpravljene(request9);
	    	System.out.println(response9.getOpravekList());
		} catch (StatusRuntimeException e) {
		    return;
		}
	}
}
